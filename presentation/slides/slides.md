---

title: Kubernetes para gente de a pie
subtitle: La Punta del Iceberg
authors: Juan Kunfoo (JSL)
email: "juanmatias@gmail.com"
revealOptions:
    transition: 'fade'

---

![JSL](./images/jinetes-01.png)

_Kubernetes para gente de a pie, la punta del Iceberg_

> Todo el fuente para esta charla la pueden encontrar en https://gitlab.com/jsl3/charla-k8s-la-punta-del-iceberg

---

<!-- .slide: data-background="./images/kubernetes-logo_250.png" data-background-size="250px" data-background-position="center" data-background-opacity="0.2" -->

## Programa

  - La App
  - La solución en contenedores
  - ¿Kuberqué?
  - Pods
  - Servicios
  - Escalabilidad
  - Ingress
  - Dashboard

---

## La App

![Basic app](./images/diagram001-basic_app.png)

Note: mostrar diagrama y ejecutar aplicación, aclarar que se puede "sobreescribir" el hostname del backend


---

## Práctica

![Hazlo](./images/doit001.gif)

---

## La App

Tener esta solución en el servidor implica (entre otros puntos):

  - Instalar entorno en el server (compiladores, paquetes, intérpretes, etc)
  - Riesgos de seguridad (si una app se cuelga puede colgar el server entero)

---

## La solución en contenedores

![Docker app](./images/diagram001-docker_app.png)

Note: crear red, levantar contenedores y referenciar por nombre


---

## Práctica

![Hazlo](./images/doit002.gif)

---

## La solución en contenedores

Llevar a contenedores permite (entre otras cosas):

  - Mover el entorno al container (el servidor sólo necesita el container engine)
  - Si una app se cuelga se cuelga el container y no el server
  - Separar lógicamente grupos de aplicaciones (con sus propios endpoints expuestos, redes, etc)

---

## ¿Kuberqué?

> Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications. It groups containers that make up an application into logical units for easy management and discovery.

---

## ¿Por qué me iría de Docker a Kubernetes?

  - No son competidores, cada cual tiene su campo
    - Docker es una solución de "conteinerización"
    - Kubernetes es un orquestador de containers
      - Coordinar y "schedulear" containers
      - Actualizar aplicaciones sin interrupción
      - Monitorizar servicios para conocer su salud
      - Escalar aplicaciones
      - ... más

---

## Herramientas: K3s

Un cluster de Kubernetes en un solo nodo y de fácil instalación.

### Commands

  - Instalar: `curl -sfL https://get.k3s.io | sh -`
  - Arrancar: `sudo systemctl start k3s.service`
  - Obtener Kubeconfig: `sudo k3s kubectl config view --raw > /tmp/kubeconfig.file`
  - Detener: `sudo systemctl stop k3s.service`

---

## Herramientas: K3s
### Commands

Para arrancar, parar, resetear y obtener el Kubeconfig pueden usar el script _k3s-boot.sh_:

`https://gitlab.com/tooling2/k3s-boot`

---

## Herramientas: Kubectl

El cliente con el que hablamos a Kubernetes

### Commands

  - Instalar: 
    - `curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl`
    - `chmod +x ./kubectl && mv kubectl ~/.local/bin`
  - Probar: `kubectl version`
  - Setear entorno para comunicarse al cluster de K3s: `export KUBECONFIG="/tmp/kubeconfig.file"`
---

## La App en Kubernetes

![Kubernetes app](./images/diagram001-kubernetes_app.png)

> Note que ya no hay un "server", el cluster de Kubernetes se puede extender sobre varios nodos (servers reales o VMs)

---

## Namespaces

![Kubernetes app](./images/diagram001-kubernetes_app.png)

### ¿Qué son?

  - Agrupación lógica de pods y otros objetos de Kubernetes
  - _Por ahora no le demos mayor importancia_
  
---

## Pods

![Kubernetes app](./images/diagram001-kubernetes_app.png)

### ¿Qué son?

  - Agrupación lógica de containers
  - Suele tener un solo container
  - Tiene nombre e IP
  - Desde dentro del POD se puede hacer referencia a localhost

---

## Pods
### ¿Cómo se definen?

  - En Kubernetes todo (no sólo los _pods_) se define a través de "manifiestos", o _"manifests"_.
  - Un manifiesto es un _yaml_
  - Se declara allí el estado deseado del objeto y se deja que Kubernetes lo alcance como le parezca

### ¿Cómo es un manifiesto?

```
apiVersion: v1
kind: Pod
metadata:
  name: backend
  labels:
    course: jsltraining
    app: backend
spec:
  containers:
    - name: backend
      image: docker.io/juanmatias/jsltalks001-backend:latest
      ports:
        - name: http
          containerPort: 8080
          protocol: TCP
```

---

# Port Forward

Antes de ir a la práctica aclaremos qué es un _port-forward_.

![Kubernetes app](./images/diagram002-kubernetes_app.png)

---

## Práctica

![Hazlo](./images/yoda.gif)

---

## Situación

De esta forma nos encontramos con dos "peros":

  - en una mano no estaríamos muy lejos de lo que hace Docker,
  - en la otra, podríamos decir que estaríamos peor (ahora tenemos que buscar el IP asignado al backend para informarle al frontend a quién consultar)

![¿Ahora qué?](./images/nemo-friends.gif)

---

## Servicios, parte 1

Vamos por partes, para lo del IP, venga un servicio.

Un Servicio es, básicamente:

> una forma de asignar un IP y un nombre persistentes a un pod o conjunto de pods.

---

## Servicios, parte 1

![Kubernetes app](./images/diagram003-kubernetes_app.png)

---

## Servicios, parte 1

![Kubernetes app](./images/diagram004-kubernetes_app.png)

---

## Servicios, parte 1

¿Cómo sabe Kubernetes a qué pod (o grupo de pods) enviar el tráfico que llega a un servicio?


<div id="left">

```
apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 80
```

</div>

<div id="right">

```
apiVersion: v1
kind: Pod
metadata:
  name: frontend
  labels:
    course: jsltraining
    app: frontend
spec:
  containers:
    - name: frontend
      image: docker.io/juanmatias/jsltalks001-frontend:latest
      ports:
        - name: http
          containerPort: 80
          protocol: TCP
```

</div>

_RoundRobin entre los pods que tengan la etiqueta marcada en selector._

---

## Práctica

![Bruce practica](./images/bruce.gif)

---

## Deployments, sobre escalabilidad y otras yerbas

Para el otro tema: _¿qué agrega ésto sobre lo que ya hace Docker?_

Veamos qué es un Deployment...

---

## Deployments, sobre escalabilidad y otras yerbas

  - Un deployment es un controlador (controla a través de un _replica set_)
  - ¿Qué controla?:
    - Cómo son creados los pods
    - Cómo son scheduleados
    - Cómo son escalados
    - Cómo son actualizados (permite seleccionar la estrategia para actualizar una serie de pods casi sin downtime: strategy)
    - ...más

---

## Escalabilidad y otras yerbas (Deployments)

¿Cómo defino un Deployment? ¡Con un manifiesto!

<div id="left">

```
apiVersion: v1
kind: Pod
metadata:
  name: frontend
  labels:
    course: jsltraining
    app: frontend
spec:
  containers:
    - name: frontend
      image: docker.io/juanmatias/jsltalks001-frontend:latest
      ports:
        - name: http
          containerPort: 80
          protocol: TCP
```

</div>


<div id="right">

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        course: jsltraining
        app: frontend
    spec:
      containers:
        - name: frontend
          image: docker.io/juanmatias/jsltalks001-frontend:latest
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
```

</div>

Note: The .spec.selector field defines how the Deployment finds which Pods to manage. In this case, you simply select a label that is defined in the Pod template (app: nginx). However, more sophisticated selection rules are possible, as long as the Pod template itself satisfies the rule.

---

## Mejor vamos a la práctica

![Bazinga](./images/bigbang.gif)

---

## Una nota sobre _ReplicaSets_ y _Deployments_

> El propósito de un ReplicaSet es mantener un grupo estable de una cantidad dada de pods en un momento dado. 

> Los _Deployments_ manejan capacidades tales como definir las estrategias de actualización.


---

## Una nota sobre _ReplicaSets_ y _Deployments_

Al ser creado, el _Deployment_, crea un _ReplicaSet_ que describe el estado deseado actual. Si se actualiza ese _Deployment_ un nuevo _ReplicaSet_ es creado con el nuevo estado deseado:

![ReplicaSet](./images/deployments-replica-sets-and-pods2.png)

_Fuente: https://www.ibm.com/cloud/architecture/content/course/kubernetes-101/deployments-replica-sets-and-pods_

---

## Servicios, parte 2

Tipos de Servicios:

  - *ClusterIP* 
    - Expone el servicio en un IP interno al cluster
  - *NodePort* 
    - Expone el servicio en un puerto específico en cada nodo del cluster
  - *LoadBalancer*
    - Expone el servicio en un IP externo, usando el LoadBalancer del provider

---

## Servicios, parte 2

![Servicios](./images/diagram001-kubernetes_services.png)

---

## Servicios, parte 2

![Servicios](./images/diagram002-kubernetes_services.png)

---

## Servicios, parte 2

![Servicios](./images/diagram003-kubernetes_services.png)

---

## Más práctica en servicios

![Servicio](./images/atyourservice.gif)

---

## Ingress

Un ingress es un nivel de abstracción que se despliega frente a un servicio.

Expone rutas HTTP y HTTPS desde fuera del cluster hacia los servicios en el mismo. Tiene reglas para definir el ruteo del tráfico.

Puede brindar a los servicios una URL visible desde el exterior, balancear tráfico, terminar conexiones SSL/TLS y ofrecer virtual hosting.

La implementación del ingress dependerá del provider.

---

## Ingress

K3s provee un tipo de Ingress Controller... así que vamos a definir un ingress para nuestro servicio ClusterIP.

¡Manifiesto!

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: frontend-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /
        backend:
          serviceName: frontend
          servicePort: 8080
```

---

## Práctica

![Practica](./images/twoidiots.gif)

---

## Preguntas

_Si no pudiste hacer tu pregunta en la presentación podés escribir a juanmatias@gmail.com._

---

## Referencias

  - https://kubernetes.io/docs/
  - https://kubernauts.io
  - https://github.com/ramitsurana/awesome-kubernetes
  - https://caylent.com/50-useful-kubernetes-tools
  - https://blog.container-solutions.com/kubernetes-deployment-strategies
  - https://github.com/benc-uk/kubeview

---

## Agradecimientos

Esta presentación es posible gracias a:

  - JSL (Jinetes del Software Libre)
  - 3XM Group (https://www.3xmgroup.com/)
  - Mariano
  - Toda la gente que hoy participó de esta reunión

---

## Acerca del software

Para realizar esta presentación se ha utilizado (entre otros softwares):

  - GNU/Linux (ArchLinux)
  - reveal.md (presentación)
  - Dia (diagramas)
  - Gimp (imagenes)
  - Neovim (editor de texto)

---

### Acerca de mi

Mi nombre es _Juan Matías de la Cámara Beovide_ pero, y a falta de que mi nombre sea largo ya, me dicen **Kungfu**.

Trabajo como DevOps y vengo de un trasfondo de desarrollador y administrador de sistemas.

Medios para contactarme:

  - juanmatias@gmail.com
  - LinedIn: https://www.linkedin.com/in/juan-matías-de-la-cámara-beovide-6703196
  - https://juanmatiasdelacamara.wordpress.com/

---

![gracias](./images/gracias.jpg)

# GRACIAS

